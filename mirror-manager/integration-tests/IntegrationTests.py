import os, sys, copy
import sh, shutil
import requests, json
import unittest, time

PROJECT_KEY = "test"
PROJECT_NAME = "integration-tests"
REPO_NAME = "mirrormanagertests"
MIRROR_REPO_NAME = REPO_NAME + "mirror"

# Bitbucket should be running at default location with default username/password
AUTH=('admin', 'admin')
BITBUCKET_REST_BASE_URL = "http://localhost:7990/bitbucket/rest/api/1.0/"
BITBUCKET_MIRROR_MANAGEMENT_BASE_URL = "http://localhost:7990/bitbucket/rest/mirror-manager/latest/"

def create_random_commit(directory, size=None, extension=None, message="random commit"):
	file_name = ""
	if extension:
		file_name = sh.mktemp("--suffix=.%s" % extension, "--tmpdir=" + directory).strip()
	else:
		file_name = sh.mktemp("--suffix=.txt", "--tmpdir=" + directory).strip()
	
	if size:
		sh.truncate("--size=%s" % size, file_name)

	git.add(os.path.basename(file_name))
	git.commit("-m", message)

def clone_repository(project_key, repo_name, clone_directory=None, mirror=False):
	CLONE_URL = "http://%s:%s@localhost:7990/bitbucket/scm/%s/%s.git" % (AUTH[0], AUTH[1], project_key, repo_name)

	git_directory = None
	if not clone_directory:
		clone_directory = repo_name

	if mirror:
		sh.git("clone", CLONE_URL, clone_directory, "--mirror")
	else:
		sh.git("clone", CLONE_URL, clone_directory)

	git_directory = os.path.join(clone_directory)

	if os.path.exists(git_directory):
		print "Cloned %s repo" % git_directory
	else:
		print "Unable to clone %s repo" % git_directory
		sys.exit(1)

	return git_directory

def setup_bitbucket_repository():
	def list_repositories_in_project(project_key):
		# Get and delete the repos in the project
		response = requests.get(BITBUCKET_REST_BASE_URL + "projects/" + project_key + "/repos", auth=AUTH)

		if response.status_code == requests.codes.ok:
			response_json = json.loads(response.text)
			return response_json["values"]
		else:
			"Unable to list repositories in %s project" % project_key
			sys.exit(1)

	def create_repository(project_key, repo_name):
		repo_data = {"name": repo_name, "scmId": "git", "forkable": True}
		response = requests.post(BITBUCKET_REST_BASE_URL + "projects/" + project_key + "/repos", json=repo_data, auth=AUTH)

		if response.status_code != requests.codes.created:
			print "Unable to create new %s repo" % repo_name
			sys.exit(1)
		else:
			print "Created new %s repo" % repo_name

	def delete_repository(project_key, repo_slug, repo_name):
		response = requests.delete(BITBUCKET_REST_BASE_URL + "projects/" + project_key + "/repos/" + repo_slug, auth=AUTH)

		if response.status_code == requests.codes.accepted:
			print "Deleted old %s repo" % repo_name

	def create_project(project_key, project_name, project_description):
		project_data = {"key": project_key, "name": project_name, "description": project_description}
		response = requests.post(BITBUCKET_REST_BASE_URL + "projects", json=project_data, auth=AUTH)

		if response.status_code != requests.codes.created:
			print "Unable to create %s project" % project_name
			sys.exit(1)
		else:
			print "Created new %s project" % project_name

	def delete_project(project_key, project_name):
		response = requests.delete(BITBUCKET_REST_BASE_URL + "projects/" + project_key, auth=AUTH)

		if response.status_code != requests.codes.no_content:
			print "Unable to delete %s project" % project_name
			sys.exit(1)
		else:
			print "Deleted old %s project" % project_name

	def project_exists(project_key):
		response = requests.get(BITBUCKET_REST_BASE_URL + "projects/" + project_key, auth=AUTH)
		return response.status_code != requests.codes.not_found

	if project_exists(PROJECT_KEY):
		for repo in list_repositories_in_project(PROJECT_KEY):
			delete_repository(PROJECT_KEY, repo["slug"], repo["name"])
		delete_project(PROJECT_KEY, PROJECT_NAME)

	create_project(PROJECT_KEY, PROJECT_NAME, "Used to run integration tests on HygieneHooks")
	create_repository(PROJECT_KEY, REPO_NAME)

def main():
	# Delete the local repo if it exists
	if os.path.exists(REPO_NAME):
		shutil.rmtree(REPO_NAME)

	if os.path.exists(MIRROR_REPO_NAME):
		shutil.rmtree(MIRROR_REPO_NAME)

	setup_bitbucket_repository()

	global git_directory
	git_directory = clone_repository(PROJECT_KEY, REPO_NAME)
	
	global gitmirror_directory
	gitmirror_directory = clone_repository(PROJECT_KEY, REPO_NAME, MIRROR_REPO_NAME, mirror=True)

	global git
	git = sh.git.bake(_cwd=git_directory)

	global gitmirror
	gitmirror = sh.git.bake(_cwd=gitmirror_directory)

	# Initial commit in repo (creates master branch)
	create_random_commit(git_directory)
	git.push("origin", "HEAD")

	# Run the unit tests
	unittest.main()

'''Superclass which handles setup/teardown common to all unit tests.'''
class SetupTests(unittest.TestCase):
	hook_key = ""

	def __init__(self, hook_key, *args, **kwargs):
		super(SetupTests, self).__init__(*args, **kwargs)
		self.hook_key = hook_key

	# Enables this hook
	@classmethod
	def setUpClass(self):
		response = requests.put(BITBUCKET_REST_BASE_URL + "projects/" + PROJECT_KEY + "/repos/" + REPO_NAME + "/settings/hooks/" + self.hook_key + "/enabled", 
			auth=AUTH)

	def get_mirrors(self):
		response = requests.get(BITBUCKET_MIRROR_MANAGEMENT_BASE_URL + "projects/" + PROJECT_KEY + "/repos/" + REPO_NAME + "/settings", auth=AUTH)

		self.assertEqual(response.status_code, requests.codes.ok)

		return json.loads(response.text)

	def add_mirror(self, mirror_name, mirror_url):
		json = {
			"name": mirror_name,
			"url": mirror_url
		}

		response = requests.put(BITBUCKET_MIRROR_MANAGEMENT_BASE_URL + "projects/"+ PROJECT_KEY + "/repos/" + REPO_NAME + "/settings", json=json, auth=AUTH)

		self.assertEqual(response.status_code, requests.codes.ok)

	def delete_mirror(self, mirror_id):
		json = {
			"id": mirror_id
		}

		response = requests.delete(BITBUCKET_MIRROR_MANAGEMENT_BASE_URL + "projects/"+ PROJECT_KEY + "/repos/" + REPO_NAME + "/settings", json=json, auth=AUTH)

		self.assertEqual(response.status_code, requests.codes.ok)

	def update_mirror(self, mirror_id, mirror_name, mirror_url, enabled):
		json = {
			"id": mirror_id,
			"name": mirror_name,
			"url": mirror_url,
			"enabled": enabled
		}

		response = requests.post(BITBUCKET_MIRROR_MANAGEMENT_BASE_URL + "projects/"+ PROJECT_KEY + "/repos/" + REPO_NAME + "/settings", json=json, auth=AUTH)

		self.assertEqual(response.status_code, requests.codes.ok)

'''MirrorManager unit tests class.'''
class MirrorManagerTests(SetupTests):
	hook_key = "com.teslamotors.bitbucket.plugins.mirror-manager:repo-mirror-pusher"

	def __init__(self, *args, **kwargs):
		super(MirrorManagerTests, self).__init__(self.hook_key, *args, **kwargs)

	def test_mirror_fast_forward_commit(self):
		self.add_mirror("gitmirror", "file://" + os.path.abspath(gitmirror_directory))

		git.checkout("master")
		create_random_commit(git_directory)
		git.push("origin", "HEAD")

		time.sleep(1)

		self.assertTrue("master" in gitmirror("branch"))
		self.assertEqual(git("rev-parse", "master").strip(), gitmirror("rev-parse", "master").strip())

		mirrors = self.get_mirrors()
		self.delete_mirror(mirrors[0]["id"])

	def test_mirror_new_branch(self):
		self.add_mirror("gitmirror", "file://" + os.path.abspath(gitmirror_directory))

		git.checkout("-b", "new_branch")
		create_random_commit(git_directory)
		git.push("origin", "HEAD")

		time.sleep(1)

		self.assertTrue("new_branch" in gitmirror("branch"))
		self.assertEqual(git("rev-parse", "new_branch").strip(), gitmirror("rev-parse", "new_branch").strip())

		mirrors = self.get_mirrors()
		self.delete_mirror(mirrors[0]["id"])

	def test_mirror_deleted_branch(self):
		self.add_mirror("gitmirror", "file://" + os.path.abspath(gitmirror_directory))

		git.checkout("-b", "delete_me")
		create_random_commit(git_directory)
		git.push("origin", "HEAD")

		time.sleep(1)

		self.assertTrue("delete_me" in gitmirror("branch"))
		self.assertEqual(git("rev-parse", "delete_me").strip(), gitmirror("rev-parse", "delete_me").strip())

		git.push("origin", ":delete_me")
		git.checkout("-")
		git.branch("-D", "delete_me")

		time.sleep(1)

		self.assertFalse("delete_me" in gitmirror("branch"))

		mirrors = self.get_mirrors()
		self.delete_mirror(mirrors[0]["id"])

	def test_mirror_rebased_commit(self):
		self.add_mirror("gitmirror", "file://" + os.path.abspath(gitmirror_directory))

		git.checkout("-b", "rebased")
		create_random_commit(git_directory)
		git.push("origin", "HEAD")

		time.sleep(1)

		self.assertTrue("rebased" in gitmirror("branch"))
		self.assertEqual(git("rev-parse", "rebased").strip(), gitmirror("rev-parse", "rebased").strip())

		git.commit("--amend", "-m", "new message")
		git.push("origin", "HEAD", "-f")

		time.sleep(1)

		self.assertTrue("rebased" in gitmirror("branch"))
		self.assertEqual(git("rev-parse", "rebased").strip(), gitmirror("rev-parse", "rebased").strip())

		git.push("origin", ":rebased")
		git.checkout("-")
		git.branch("-D", "rebased")

		time.sleep(1)

		self.assertFalse("rebased" in gitmirror("branch"))

		mirrors = self.get_mirrors()
		self.delete_mirror(mirrors[0]["id"])

	def test_add_mirror(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror", "file:///tmp/mirror.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.delete_mirror(mirrors[num_mirrors]["id"])

	def test_delete_mirror(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror2", "file:///tmp/mirror2.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.delete_mirror(mirrors[num_mirrors]["id"])

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors)

	def test_update_mirror_name(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror3", "file:///tmp/mirror3.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.update_mirror(mirrors[num_mirrors]["id"], "new_name1", "new_url1", False)

		mirrors = self.get_mirrors()
		mirror_that_changed = mirrors[num_mirrors]

		self.assertEqual(mirror_that_changed["name"], "new_name1")

		self.delete_mirror(mirror_that_changed["id"])

	def test_update_mirror_url(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror4", "file:///tmp/mirror4.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.update_mirror(mirrors[num_mirrors]["id"], "new_name2", "new_url2", False)

		mirrors = self.get_mirrors()
		mirror_that_changed = mirrors[num_mirrors]

		self.assertEqual(mirror_that_changed["url"], "new_url2")

		self.delete_mirror(mirror_that_changed["id"])

	def test_update_mirror_enabled(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror5", "file:///tmp/mirror5.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.update_mirror(mirrors[num_mirrors]["id"], "new_name3", "new_url3", False)

		mirrors = self.get_mirrors()
		mirror_that_changed = mirrors[num_mirrors]

		self.assertEqual(mirror_that_changed["enabled"], False)

		self.delete_mirror(mirror_that_changed["id"])

	def test_add_duplicate_mirror_url(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror6", "file:///tmp/mirror6.git")
		self.add_mirror("gitmirror7", "file:///tmp/mirror6.git")
		self.add_mirror("gitmirror8", "file:///tmp/mirror6.git")
		self.add_mirror("gitmirror9", "file:///tmp/mirror6.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 1)

		self.delete_mirror(mirrors[num_mirrors]["id"])

	def test_update_duplicate_mirror_url(self):
		mirrors = self.get_mirrors()
		num_mirrors = len(mirrors)

		self.add_mirror("gitmirror10", "file:///tmp/mirror7.git")
		self.add_mirror("gitmirror11", "file:///tmp/mirror8.git")

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 2)

		self.update_mirror(mirrors[num_mirrors]["id"], "new_name4", "file:///tmp/mirror8.git", False)

		mirrors = self.get_mirrors()
		self.assertEqual(len(mirrors), num_mirrors + 2)

		# Update should not work since their would be 2 mirrors with same url
		self.assertNotEqual(mirrors[num_mirrors]["url"], mirrors[num_mirrors + 1]["url"])

		self.delete_mirror(mirrors[num_mirrors]["id"])
		self.delete_mirror(mirrors[num_mirrors + 1]["id"])


if __name__ == "__main__":
	main()