define('plugin/mirror-ops/repository-settings', [
  'jquery',
  'aui',
  'bitbucket/internal/util/ajax',
  'bitbucket/internal/model/page-state',
  'bitbucket/internal/util/error',
  'exports'
  ], function (
          $,
          AJS,
          ajax,
          pageState,
          errorUtil,
          exports
  ) {

	function getProjectKey() {
		return pageState.getProject().getKey();
	}

	function getRepoSlug() {
		return pageState.getRepository().getSlug();
	}

    function getUrlBase() {
        return AJS.contextPath() + '/rest/mirror-manager/latest/projects/' + getProjectKey() + '/repos/' + getRepoSlug();
    }

	function fetchMirrorList(mirrors_box) {

		$.getJSON( getUrlBase() + '/settings', {},
			function(mirrors) {

                $.each(mirrors, function(index, mirror) {
                    var mirror_id = mirror.id;
                    var mirror_name = mirror.name;
                    var mirror_url = mirror.url;
                    var mirror_enabled = mirror.enabled;

                    var single_mirror_div = $("<form>", {"class": "aui", "style": "margin-top: 1.5em"});

                    var single_mirror_name = $("<h3>", {"class": "description"});
                    single_mirror_name.text(mirror_name);
                    single_mirror_div.append(single_mirror_name);

                    var single_mirror_url = $("<div>", {"class": "description", "style": "font-weight: bold"});
                    single_mirror_url.text(mirror_url);
                    single_mirror_div.append(single_mirror_url);

                    var single_mirror_enabled = $("<div>", {"class": "description", "style": "font-weight: bold"});
                    if (mirror_enabled) {
                        single_mirror_enabled.text("Enabled").css("color", "green");
                    } else {
                        single_mirror_enabled.text("Disabled").css("color", "red");
                    }
                    single_mirror_div.append(single_mirror_enabled);

                    var $mirror_name_textbox = $("<input>", {"class": "text mirror-manager-textbox"}).val(mirror_name);
                    var $mirror_url_textbox = $("<input>", {"class": "text mirror-manager-textbox"}).val(mirror_url);

                    var $mirror_checkbox_div = $("<div>", {"class": "checkbox"});

                    var checkbox_id = "id-mirror-" + mirror_id;
                    var checkbox_name = "name-mirror-" + mirror_name;

                    var $mirror_checkbox = $("<input>", {"id": checkbox_id, "class": "checkbox", "type": "checkbox", "name": checkbox_name});
                    if (mirror_enabled)
                        $mirror_checkbox.attr("checked", "checked");

                    var $mirror_checkbox_label = $("<label>", {"for": checkbox_id});
                    $mirror_checkbox_label.text("Enable automatic post-receive pushing");

                    $mirror_checkbox_div.append($mirror_checkbox);
                    $mirror_checkbox_div.append($mirror_checkbox_label);

                    var update_button = $("<button>", {"class": "aui-button", "type": "button"});
                    update_button.text("Update");
                    single_mirror_div.append(update_button);

                    var cancel_update_button = $("<button>", {"class": "aui-button", "type": "button"});
                    cancel_update_button.text("Cancel");
                    cancel_update_button.hide();
                    cancel_update_button.click(function() {
                        renderMirrorList();
                    });
                    single_mirror_div.append(cancel_update_button);

                    update_button.click(function() {
                        if (update_button.text() === "Update") {
                            single_mirror_name.replaceWith($mirror_name_textbox);
                            single_mirror_url.replaceWith($mirror_url_textbox);
                            single_mirror_enabled.replaceWith($mirror_checkbox_div);

                            update_button.text("Save");
                            cancel_update_button.show();
                        } else if (update_button.text() === "Save") {
                            $.ajax( getUrlBase() + '/settings',
                                {
                                    type: "POST",
                                    data: JSON.stringify({
                                        "id": mirror_id,
                                        "name": $mirror_name_textbox.val(),
                                        "url": $mirror_url_textbox.val(),
                                        "enabled": $mirror_checkbox.attr("checked") === "checked"
                                    }),
                                    contentType: "application/json",
                                    success: function(data) {
                                        renderMirrorList();
                                    }
                                }
                            );
                        }
                    });

                    var deletion_button = $("<button>", {"class": "aui-button", "type": "button"});
                    deletion_button.text("Remove");
                    deletion_button.click(function() {
                        if (confirm("Are you sure you want to remove this mirror?")) {
                            $.ajax(
                                getUrlBase() + '/settings',
                                {
                                    type: "DELETE",
                                    data: JSON.stringify({
                                        "id": mirror_id,
                                        "name": mirror_name,
                                        "url": mirror_url
                                    }),
                                    contentType: "application/json",
                                    success: function(data) {
                                        renderMirrorList();
                                    }
                                }
                            );
                        }
                    });
                    single_mirror_div.append(deletion_button);

                    var manual_push_button = $("<button>", {"class": "aui-button", "type": "button"});
                    manual_push_button.text("Manual push");
                    manual_push_button.click(function() {
                        $.ajax(
                            getUrlBase() + '/manualPush',
                            {
                                type: "PUT",
                                data: JSON.stringify({
                                    "id": mirror_id,
                                    "name": mirror_name,
                                    "url": mirror_url
                                }),
                                contentType: "application/json",
                                success: function(data) {
                                    alert("Initiated asynchronous push to mirror: " + mirror_name);
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    alert("Failed in pushing to mirror: " + errorThrown);
                                }
                            }
                        );
                    });
                    single_mirror_div.append(manual_push_button);


                    mirrors_box.append(single_mirror_div);
                });
            }
		);
	}

	function renderMirrorList() {

		var mirrors_box = $("#mirror-selection");
		mirrors_box.empty();
		fetchMirrorList(mirrors_box);
	}

	function initAdderButtonBehavior() {
		var adder_button = $("#addMirror-button");
		adder_button.click(function() {

			var mirror_name = prompt("Choose an alias for this remote mirror:", "my_mirror")
			var mirror_url = prompt("Specify URL remote mirror: \"" + mirror_name + "\"", "file:///tmp/stash/mirror-test.git")	
			$.ajax({
			    url: getUrlBase() + "/settings",
			    type: "PUT",
			    data: JSON.stringify({
                    "name": mirror_name,
                    "url": mirror_url
                }),
                contentType: "application/json",
                success: function(data) {
                    renderMirrorList();
                }
			});
		});
	}

	exports.onReady = function () {

		initAdderButtonBehavior();

		// FIXME Why is this required?
		setTimeout(function() {
			renderMirrorList();
		}, 0);
	}
});
