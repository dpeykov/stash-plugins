package com.teslamotors.bitbucket.plugins.settings;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.Table;

@Table("MIRROR_SETTINGS")
public interface MirrorSettings extends Entity {

    @Mutator("PROJECT")
    void setProject(String project);

    @Accessor("PROJECT")
    String getProject();

    @Mutator("REPO")
    void setRepo(String repo);

    @Accessor("REPO")
    String getRepo();

    @Accessor("NAME")
    public String getName();

    @Mutator("NAME")
    public void setName(String name);

    @Accessor("URL")
    public String getUrl();

    @Mutator("URL")
    public void setUrl(String url);

    @Accessor("ENABLED")
    public Boolean getEnabled();

    @Mutator("ENABLED")
    public void setEnabled(Boolean enabled);
}