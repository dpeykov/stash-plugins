Force Push Branch Portector
============================

Prevents force pushes to branches specified by a config file. The hook configuration lets
you choose the branch and path at which the config file resides.

You may want to create an orthogonal branch in your repository with its own branch permissions
(e.g. admins only) to store this config file.

A suggested name for the hook file is `forcepush.config`.

The config file format is a list of regular expressions that will be tested against branch
names for force push rejection.

An example file:

    refs/heads/master
    refs/heads/another-specific-branch
    refs/heads/noforce/.*

Note that the lines represet *regular expressions*, not *glob expressions*. The following line won't
do what you expect:

    refs/heads/noforce/*

You need the period, like this:

    refs/heads/noforce/.*

TODO
============================

* Re-instate unit tests

Test Case
============================

1. Create a test repo with three branches that resembles the following branch and commit structure:

	      ______ Branch1
	_____/______ Master
	 \   \______ Branch2
	  \______ force_push
	  
	The force push branch contains the config file naming the branches where force push is disabled.

2. Without the plugin turned on, attempt to push branch1 to master: git push origin branch1:master
	*Expected Result: **Failure**
2. Enable the hook by specifying refs/heads/master in the config file and supplying the config file path.
3. Attempt to force push branch1 to master: git push origin branch1:master --force
	*Expected Result: **Failure**
4. Disable the plugin and again force push branch1 to master: git push origin branch1:master --force
	*Expected Result: **Success**
5. Modify config file and add "refs/heads/.*"
6. Enable plugin, attempt to force push branch1 to branch2: git push origin branch1:branch2 --force
	*Expected Result: **Failure**
7. Remove the previous change to the config file and attempt to force push again: git push origin branch1:branch2 --force
	*Expected Result: **Success**


Acknowledgements
============================

[This project on Bitbucket](https://bitbucket.org/mark0815/stash-reject-force-push-hook) was used as a template to get started.
