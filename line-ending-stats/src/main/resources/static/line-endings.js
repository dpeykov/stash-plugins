
define('plugin/line-endings',
		['exports', 'jquery', 'lodash', 'aui'],
		function(exports, $, _, AJS) {

		function printObject(obj) {
			$.each(obj, function(key, element) {
				console.log('key: ' + key + '\n' + 'value: ' + element);
			});
		}

		var pageState = require('bitbucket/internal/model/page-state');
	
		function getProjectKey() {
			return pageState.getProject().getKey();
		}

		function getRepoSlug() {
			return pageState.getRepository().getSlug();
		}
		
		function getUrlBase() {
			return AJS.contextPath() + '/rest/line-endings/latest';
		}
		
		exports.labelfunc = function(context) {

			var data;
			var succeeded = false;
			$.ajax({
				type: 'GET',
				url: getUrlBase() + '/api',
				dataType: 'json',
				success: function(data_result) {
					data = data_result;
					succeeded = true;
				},
				data: {
					project_key: getProjectKey(),
					repo_slug: getRepoSlug(),
					file_ref: context.fileChange.commitRange.untilRevision.id,
					file_path: context.fileChange.path.components.join("/")
				},
				async: false
			});
			
			if (!succeeded)
				return "<no file>";

			var total_counts = data.lf_count + data.crlf_count;
			var lf_percent = Math.floor(100.0*data.lf_count/total_counts);
			var crlf_percent = Math.floor(100.0*data.crlf_count/total_counts);

			var label_string;
			if (total_counts == 0) {
				label_string = "No line endings";
			} else if (data.crlf_count == 0) {
				label_string = "Pure LF (Unix-style)";
			} else if (data.lf_count == 0) {
				label_string = "Pure CRLF (Windows-style)";
			} else {
				label_string = "Mixed line endings: " + data.lf_count + "/" + total_counts + " (" + lf_percent + "%) LF, " + data.crlf_count + "/" + total_counts + " (" + crlf_percent + "%) CRLF";
			}

			return label_string;
		}
	}
);
