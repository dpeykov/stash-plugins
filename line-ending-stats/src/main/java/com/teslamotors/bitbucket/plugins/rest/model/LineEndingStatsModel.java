package com.teslamotors.bitbucket.plugins.rest.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class LineEndingStatsModel {

    @JsonProperty
    final private int lf_count;
    
    @JsonProperty
    final private int crlf_count;
    
    
    public LineEndingStatsModel(int lf_count, int crlf_count) {
        this.lf_count = lf_count;
        this.crlf_count = crlf_count;
    }
}