package com.teslamotors.bitbucket.plugins.rest.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class LineEndingCounter {

	// XXX
	static final boolean DEBUG = false;

	final static char CARRIAGE_RETURN = '\r';
	final static char LINE_FEED = '\n';

	enum LineEnding {
		WINDOWS,	// CRLF
		UNIX		// LF
	}

	public static LineEndingStatsModel check(InputStream is) throws IOException {

		int lf_count = 0;
		int crlf_count = 0;
		
		Reader reader = new BufferedReader(new InputStreamReader(is));

		int character;
		boolean last_element_was_carriage_return = false;
		int line_number = 1;
		
		StringBuilder line = new StringBuilder();
		while (0 <= (character = reader.read())) {

			switch (character) {
			case CARRIAGE_RETURN:
			{	
				last_element_was_carriage_return = true;
				break;
			}
			case LINE_FEED:
			{
				LineEnding current_line_ending = last_element_was_carriage_return ? LineEnding.WINDOWS : LineEnding.UNIX;

				if (DEBUG) {
					System.out.println( String.format("Found %s line ending at line %d in file",
							current_line_ending.name(),
							line_number
							));
				}
			
				switch(current_line_ending) {
				case WINDOWS:
					crlf_count++;
					break;
				case UNIX:
					lf_count++;
					break;
				}

				line_number++;
				line.setLength(0);
				last_element_was_carriage_return = false;
				break;
			}	
			default:	// Neither a line feed nor carriage return
			{
				line.append((char) character);
				last_element_was_carriage_return = false;
				break;
			}
			}
		}
		
		return new LineEndingStatsModel(lf_count, crlf_count);
	}
}
