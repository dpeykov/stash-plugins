Plugin Implementation
======================================

[TOC]

At my workplace, we use Atlassian Bitbucket as our authoritative Git host. I though I might have a go at creating a plugin to augment pull requests with interdiff links, since the Bitbucket developers don't appear to be working on it (see [STASH-2896](https://jira.atlassian.com/browse/STASH-2896)).


Stash 2.8 Update
----------------------------
In Stash 2.8, an API function for programmatically validating the exact content of the pull request merge result was introduced (see [STASH-3122](https://jira.atlassian.com/browse/STASH-3122?focusedCommentId=508205&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-508205)). This somewhat mitigates the need for fast-forward-only merges, if developers are worried only about semantic merge
errors that can be caught programmaticaly.

However, even with this new merge API feature, the motivation remains for incremental diffs over the course of an iterative review.

API Functions
-----------------------------
Bitbucket provides an API method for [querying pull request activity](https://developer.atlassian.com/static/rest/stash/2.7.2/stash-rest.html#idp2512448). Within the JSON response, some of the actions it lists are classified as the `RESCOPED` action type. These rescope actions contain the four properties:

* Target branch
    * `toHash`
    * `previousToHash`
* Feature branch
    * `fromHash`
    * `previousFromHash`

In this nomenclature, `from` means the **feature branch**, and `to` means the **target branch** of the pull request.

These four fields could be exactly sufficient information to compute the **dynamic base** interdiff as described above. However, through experimentation I've observed that the `previousToHash` and `toHash` fields do not quite satisfy this purpose.

### Rescope activity logs
Let's say that we based our feature branch upon commit `A` in the target branch. We make a commit `X` to our feature branch, push both branches, then open a pull request from branch `feature` to branch `target`.

#### Adding a commit to the feature branch
If we subsequently make a commit `Y` to branch `feature` and push, a `RESCOPE` action is generated in the activity log, with the following values:

    previousToHash     A
    toHash             A
    previousFromHash   X
    fromHash           Y

#### Adding a commit to the feature branch after a commit to the target branch

Given the initial state of the pull request where `target` = `A` and `feature` = `X`, say we make a commit `B` to the `target` branch, then push. This generates no activity in the pull request, as observed by refreshing the page/re-querying the API.

If we then make commit `Y` to branch `feature`, the `RESCOPE` action entry contains:

    previousToHash     B
    toHash             B
    previousFromHash   X
    fromHash           Y

Note that `previousToHash` and `toHash` are identical. This indicates that the activity log considers only the change to branch `feature` as part of the rescope.

#### Pushing both branches atomically

If, instead, we push both the `target` and `feature` branch in a single command, we see an update between `previousToHash` and `toHash`.

    previousToHash     A
    toHash             B
    previousFromHash   X
    fromHash           Y

The order of branches specified on the command line does not matter.

#### Rebasing the feature branch

Now to consider the value of this API for computing interdiffs. Starting again at the initial state, commit `B` is made to `target` and pushed. Next, `feature` is rebased on `B` such that commit `X` becomes `X'`. Since the `target` and `feature` branches were not pushed atomically (as will usually be the case), we observe in the activity log:

    previousToHash     B
    toHash             B
    previousFromHash   X
    fromHash           X'

### Using Rescope data effectively

For a rebase, the algorithm is:

Among the list of "removed commits", find the greatest ancestor. This is guaranteed to be the previous base commit.  Additionally, bitbucket orders commits by ancestry, so we can simply find the last one in the list. We use the parent of this in our diff.

Likewise, the new base commit will be the last one in the list of "added" commits. We use the parent of this in our diff.

Consult the [plugin implementation](https://bitbucket.org/kostmo/stash-plugins/src/f63544ee4cb31b479edd4c2013569b7e00cf1d58/interdiff/src/main/resources/static/feature/pullRequest/conflicts/pull-request-conflicts.js?at=dox#cl-158) for details.

### Conclusion

The `toHash` and `previousToHash` fields are ultimately not useful for interdiff computation, as they will not necessarily reflect the old base and new base of the feature branch upon a rebase.

However, also present in the activity logs are exhaustive lists of *added* and *removed* commits. We may be able to use these instead.


Diff hosting
----------------------------

The Bitbucket web interface cannot, as of this writing, be wrangled into displaying diffs between two arbitrary commits (see [STASH-2550](https://jira.atlassian.com/browse/STASH-2550)). Otherwise, it could have handled the display of the **static base commit** interdiffs.

The **dynamic base commit** interdiffs are another story, and would require a custom diff generation process anyway. The plugin could execute `interdiff` via the shell, capture the output, and use some Javascript diff rendering library to display it.

Garbage collection
------------------
Since we host a mirror of the repo on another server to support various other Git frontends like Gitweb and cGit, I had considered just generating links to diffs (at least for the **static base commit** case) on that server. As it happens, the pre-rebase commits do not propagate to the mirror server when executing `push --mirror`, apparently because no refs can reach them (they are "dangling").

Those commits I tested with did still exist on the Bitbucket server, however, so I guess it was just a matter of not yet being garbage-collected. Bitbucket could preserve those commits, if need be, by creating refs for them.

Implementation Result
---------------------

A prototype has been implemented that uses the `interdiff` command.

Incremental review is a feature that could win many converts from Gerrit. If you are a Bitbucket user and you agree, [vote for this issue](https://jira.atlassian.com/browse/STASH-2896)!

Related Work/Links
================================
* Question on Atlassian Answers: [How to obtain full list of added/removed commits (more than 5) in a Pull Request activity query](https://answers.atlassian.com/questions/219771/how-to-obtain-full-list-of-added-removed-commits-more-than-5-in-a-pull-request-activity-query)
