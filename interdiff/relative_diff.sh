#!/bin/bash

REPO=$1
OLD_BASE=$2
OLD_TIP=$3
NEW_BASE=$4
NEW_TIP=$5

interdiff <(git --git-dir=$REPO diff $OLD_BASE $OLD_TIP) <(git --git-dir=$REPO diff $NEW_BASE $NEW_TIP) | diff2html.py --exclude-html-headers --algorithm 2
